<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php

    require_once("Animal.php");

    class Ape extends Animal {
        public function yell() {
            echo "Yell: Auooo<br>";
        }

        // Override property legs
        public $legs = 2;
    }
    ?>
</body>
</html>