<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php

require_once("Animal.php");
require_once("Frog.php");
require_once("Ape.php");

$sheep = new Animal("shaun");
$sheep->displayInfo();
echo PHP_EOL;

$kodok = new Frog("buduk");
$kodok->displayInfo();
$kodok->jump();
echo PHP_EOL;

$sungokong = new Ape("Kera Sakti");
$sungokong->displayInfo();
$sungokong->yell();
echo PHP_EOL;
?>
    
</body>
</html>